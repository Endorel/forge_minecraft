package com.dan.perdan.proxy;
import com.dan.perdan.block.PerDanBlock;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
public class ClientProxy extends ServerProxy {

	@Override
	public void preInit (FMLPreInitializationEvent E)
	{

	}

	@Override
	public void init (FMLInitializationEvent E)
	{
		PerDanBlock.addRenders ( );
	}

	@Override
	public void postInit (FMLPostInitializationEvent E)
	{

	}
	
}
