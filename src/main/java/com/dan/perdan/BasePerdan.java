package com.dan.perdan;
import com.dan.perdan.block.PerDanBlock;
import com.dan.perdan.proxy.ServerProxy;
import com.dan.perdan.recipe.perdanrecipe;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod (modid = "perdan", name="Super-puper mod", version = "0.1")
public class BasePerdan {
	public static final String MODID = "perdan";
	public static final String VERSION = "0.1";
	@SidedProxy (clientSide = "com.dan.perdan.proxy.ClientProxy", serverSide = "com.dan.perdan.proxy.ServerProxy")
	public static ServerProxy proxy;
	@EventHandler
	public void preInit (FMLPreInitializationEvent E)
	{
		
		PerDanBlock.preInit ( );
		perdanrecipe.preInit ( );
	  proxy.preInit (E);
	}

	@EventHandler
	public void init (FMLInitializationEvent E)
	{
	  proxy.init (E);
	}

	@EventHandler
	public void postInit (FMLPostInitializationEvent E)
	{
	  proxy.postInit (E);
	}

	}

	

