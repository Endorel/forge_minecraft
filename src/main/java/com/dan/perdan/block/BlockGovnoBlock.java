package com.dan.perdan.block;

import com.dan.perdan.BasePerdan;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;

public class BlockGovnoBlock extends Block {

	public BlockGovnoBlock() {
		super(Material.GLASS);
		setUnlocalizedName ("govno_block");
		
		  setCreativeTab (CreativeTabs.BUILDING_BLOCKS);
		  setHardness (2);
		  setResistance (1);
	}

}
