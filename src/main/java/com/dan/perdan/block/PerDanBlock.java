package com.dan.perdan.block;

import com.dan.perdan.BasePerdan;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class PerDanBlock {
	
	public static Block govno_block;
	
	public static void preInit ( )
	{
		govno_block=new BlockGovnoBlock();
	 addBlocks();
	// addRenders();
	}

	public static void addBlocks ( )
	{
		addBlock (govno_block,"govno_block");
	}

	public static void addRenders ( )
	{
		addRender (govno_block,"govno_block");
	}

	public static void addBlock (Block block, String name) 
	{
	  GameRegistry.register (block, new ResourceLocation (BasePerdan.MODID, name));
	  GameRegistry.register (new ItemBlock (block), new ResourceLocation (BasePerdan.MODID, name));
	}

	public static void addRender (Block block, String name)
	{
	  Minecraft.getMinecraft ( ).getRenderItem ( ).getItemModelMesher ( ).register (Item.getItemFromBlock (block),
	    0, new ModelResourceLocation (BasePerdan.MODID + ":" + name, "inventory"));
	}
}
